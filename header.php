<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Omar Tuffaha">
    <meta name="contact" content="omar_tuffaha@hotmail.com">
    <meta name="description" content="<?php echo $dicription;?>">
    <meta property="og:image" content="">
    <meta property="og:description" content="<?php echo $dicription;?>">
    <meta property="og:title" content="<?php echo $title;?>">
    <meta name="copyright" content="&copy; 2018 Omar Tuffaha" />
    <title><?php echo $title;?></title>
    <?php
        include_once 'styles.php';
     ?>
</head>
<body>
    <div class="body-container">
        <header class="body-header">
            <div class="header-logo">
                <h1>E-ink</h1>
                <h2></h2>
            </div>
            <div class="header-other">

            </div>
        </header>
