<?php

include_once 'includes/image_functions.php';
$img = $_POST['imgBase64'];
$img = str_replace('data:image/png;base64,', '', $img);
$img = str_replace(' ', '+', $img);
$data = base64_decode($img);
$file = 'img/image_to_convert.png';
$success = file_put_contents($file, $data);
$image = imagecreatefrompng('img/image_to_convert.png');
$image = imagescale($image, 480, 800);
imagefilter($image, IMG_FILTER_CONTRAST, 90);
$eink = new E_Ink_Screen();
$eink->convertImageToBlackAndWhite($image);
imagepng($image,'img/after.png');

//writing to a file
$file = fopen('img/testApi.epd', "wb");
$array = $eink->getEPDimageDataArray($image);
foreach ($array as $byte) {
    fwrite($file, chr($byte));
}
fclose($file);
 ?>
