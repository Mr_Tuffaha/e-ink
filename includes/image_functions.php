<?php

class E_Ink_Screen {

    public function writeImageToFile($content, $path) {
        $file = fopen($path, "wb");
        foreach ($fileArray as $byte) {
            fwrite($file, chr($byte));
        }
        fclose($file);
    }

    public function splitPackets($epdImageArray) {
        for ($i = 0; $i < 191; $i++) {
            $row = array();
            for ($j = 0; $j < 251; $j++) {
                $row[] = $epdImageArray[$j + ($i * 251)];
            }
            $packet[] = array("size" => 251, "content" => $row);
        }

        $row = array();
        for ($i = 47941; $i < 48016; $i++) {
            $row[] = $epdImageArray[$i];
        }
        $packet[] = array("size" => 75, "content" => $row);
        return $packet;
    }

    public function getEPDimageDataArray($image) {

        $image = $this->getBlackAndWhitePixelArrayOfImage($image);
        $image = $this->reorderImageByteArray($image);
        $image = $this->createEPDImageContentArray($image);
        $headers = $this->getEinkHeader();
        $result = array();
        foreach ($headers as $byte) {
            array_push($result, $byte);
        }
        foreach ($image as $row) {
            foreach ($row as $byte) {
                array_push($result, $byte);
            }
        }

        return $result;
    }

    public function convertImageToBlackAndWhiteShade($image, $shades, $invert) {
        $pixelArray = array();
        $pixelRow = array();
        $shadeValue = floor(255 / $shades);
        $imageWidth = imagesx($image);
        $imageHeight = imagesy($image);

        for ($i = 0; $i < $imageHeight; $i++) {
            $pixelRow = array();
            for ($j = 0; $j < $imageWidth; $j++) {

                // Get the RGB value for current pixel

                $rgb = ImageColorAt($image, $j, $i);

                // Extract each value for: R, G, B

                $red = ($rgb >> 16) & 0xFF;
                $green = ($rgb >> 8) & 0xFF;
                $blue = $rgb & 0xFF;

                // Get the value from the RGB value

                $gray = round(($red + $green + $blue ) / 3);
                $colorChoice = floor($gray / $shadeValue) % 2;
                // Gray-scale values have: R=G=B=G
                if (!$invert) {
                    if ($colorChoice) {
                        $gray = 0xFF;
                    } else {
                        $gray = 0x00;
                    }
                } else {
                    if ($colorChoice) {
                        $gray = 0x00;
                    } else {
                        $gray = 0xFF;
                    }
                }

                $gray = $gray > 0x7F ? 0xFF : 0x00; //you can also use 0x3F 0x4F 0x5F 0x6F its on you

                $pixelValue = imagecolorallocate($image, $gray, $gray, $gray);

                // Set the gray value
                imagesetpixel($image, $j, $i, $pixelValue);
                array_push($pixelRow, $gray);
            }
            $pixelArray[] = $pixelRow;
        }
        return array('image' => $image, 'array' => $pixelArray);
    }

    public function convertImageToBlackAndWhite($image,$invert=0) {

        $imageWidth = imagesx($image);
        $imageHeight = imagesy($image);

        for ($i = 0; $i < $imageHeight; $i++) {
            $pixelRow = array();
            for ($j = 0; $j < $imageWidth; $j++) {

                // Get the RGB value for current pixel

                $rgb = ImageColorAt($image, $j, $i);

                // Extract each value for: R, G, B

                $red = ($rgb >> 16) & 0xFF;
                $green = ($rgb >> 8) & 0xFF;
                $blue = $rgb & 0xFF;

                // Get the value from the RGB value

                $gray = round(($red + $green + $blue ) / 3);

                // Gray-scale values have: R=G=B=G

                //$gray = $gray > 0x7F ? 0xFF : 0x00; //you can also use 0x3F 0x4F 0x5F 0x6F its on you
                if(!$invert){
                    $gray = $gray > 0x7F ? 0xFF : 0x00; //you can also use 0x3F 0x4F 0x5F 0x6F its on you
                }else{
                    $gray = $gray > 0x7F ? 0x00 : 0xFF; //you can also use 0x3F 0x4F 0x5F 0x6F its on you
                }

                $pixelValue = imagecolorallocate($image, $gray, $gray, $gray);

                // Set the gray value
                imagesetpixel($image, $j, $i, $pixelValue);
            }
        }
        return $image;
    }

    //end of convertImageToBlackAndWhite()

    public function getBlackAndWhitePixelArrayOfImage($image) {
        $pixelArray = array();
        $pixelRow = array();
        $imageWidth = imagesx($image);
        $imageHeight = imagesy($image);

        for ($i = 0; $i < $imageHeight; $i++) {
            $pixelRow = array();
            for ($j = 0; $j < $imageWidth; $j++) {

                // Get the RGB value for current pixel

                $rgb = ImageColorAt($image, $j, $i);

                // Extract each value for: R, G, B

                $red = ($rgb >> 16) & 0xFF;
                $green = ($rgb >> 8) & 0xFF;
                $blue = $rgb & 0xFF;

                // Get the value from the RGB value

                $gray = round(($red + $green + $blue ) / 3);

                // Gray-scale values have: R=G=B=G
                //$gray = $gray > 0x7F ? 0 : 1; //you can also use 0x3F 0x4F 0x5F 0x6F its on you

                $gray = $gray > 0x7F ? 0 : 1; //you can also use 0x3F 0x4F 0x5F 0x6F its on you



                array_push($pixelRow, $gray);
            }
            $pixelArray[] = $pixelRow;
        }
        return $pixelArray;
    }

    public function reorderImageByteArray($byteArray) {

        // 0,6,0 1,14,7,8,1 2,4,2 3,12,5,10,3 9,9 11,11 15,15
        for ($i = 0; $i < sizeof($byteArray); $i++) {
            for ($j = 0; $j < sizeof($byteArray[$i]); $j += 16) {
                $temp = $byteArray[$i][$j + 0];
                $byteArray[$i][$j + 0] = $byteArray[$i][$j + 6];
                $byteArray[$i][$j + 6] = $temp;

                $temp = $byteArray[$i][$j + 1];
                $byteArray[$i][$j + 1] = $byteArray[$i][$j + 14];
                $byteArray[$i][$j + 14] = $byteArray[$i][$j + 7];
                $byteArray[$i][$j + 7] = $byteArray[$i][$j + 8];
                $byteArray[$i][$j + 8] = $temp;

                $temp = $byteArray[$i][$j + 2];
                $byteArray[$i][$j + 2] = $byteArray[$i][$j + 4];
                $byteArray[$i][$j + 4] = $temp;

                $temp = $byteArray[$i][$j + 3];
                $byteArray[$i][$j + 3] = $byteArray[$i][$j + 12];
                $byteArray[$i][$j + 12] = $byteArray[$i][$j + 5];
                $byteArray[$i][$j + 5] = $byteArray[$i][$j + 10];
                $byteArray[$i][$j + 10] = $temp;
            }
        }
        return $byteArray;
    }

    public function createEPDImageContentArray($byteArray) {
        $row = array();
        $tempByte = array();
        for ($i = 0; $i < sizeof($byteArray); $i++) {
            for ($j = 0; $j < sizeof($byteArray[$i]); $j++) {
                if (($j != 0) && ($j % 16 == 0)) {
                    //                    echo sizeof($tempByte);
                    $twoBytes = $this->getByteValueFrom16BitArray($tempByte);
                    //                    echo json_encode($twoBytes);
                    $row[(60 - ($j / 16))] = $twoBytes[1];
                    $row[(30 - ($j / 16))] = $twoBytes[0];
                    $tempByte = array();
                    array_push($tempByte, $byteArray[$i][$j]);
                } else {
                    array_push($tempByte, $byteArray[$i][$j]);
                }
            }
            $twoBytes = $this->getByteValueFrom16BitArray($tempByte);
            $row[(30 - ($j / 16))] = $twoBytes[0];
            $row[(60 - ($j / 16))] = $twoBytes[1];
            $tempByte = array();
            ksort($row);
            $byteArray[$i] = $row;
            $row = array();
        }
        return $byteArray;
    }

    public function getByteValueFrom16BitArray($bitArray) {
        $value = [0, 0];
        for ($i = 7; $i >= 0; $i--) {
            $value[0] += $bitArray[$i] * pow(2, 7 - $i);
            $value[1] += $bitArray[$i + 8] * pow(2, 7 - $i);
        }
        return $value;
    }

    public function getEinkHeader() {
        return [0x3A, 0x01, 0xE0, 0x03, 0x20, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];
    }

}

//end of class
