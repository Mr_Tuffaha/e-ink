<?php

/**
 * @ClassName : Autologin Class
 * @Description : This Class is Used to handle the Autologin
 * @Version : 1.0v
 * @LastEdit : 03/Dec/2017
 * @Author : Omar Tuffaha <omar_tuffaha@hotmail.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 */

require_once __DIR__.'/Database.php';


class Autologin extends Database {

    
    /**
     * gets the user id and inserts an autologin token in the database and in the cookie to be checked later
     * @param type $user_identifier
     * @return (int or string) either the last inserted id to the database or mysql error
     */
    public function saveToken($user_identifier) {
        //create unique user token
        $user_token = uniqid(rand(), TRUE);
        //hash the token 
        $hashed_token = password_hash($user_token, PASSWORD_DEFAULT);
        $query = "INSERT INTO `session_remembered`(`session_remembered_user_id`, `session_remembered_token`) VALUES ('$user_identifier','$hashed_token');";
        if (!$this->performQuery($query)) {
            return $this->getMysqliError();
        } else {
            setcookie("autologin", "$user_identifier-$user_token", time() + 60 * 60 * 24 * 365, COOKIE_DIR, DOMAIN);
        }
        $lastId = $this->lastInsertedId();
        //it returns the last created id if it was created else false is returned
        return $lastId ? array($lastId, $user_identifier) : FALSE;
    }

    
    /**
     * gets autologin cookie and checks if its valid
     * if not delete all tokens for the user and sets the session hijack flag 1 and returns false.
     * else if its valid return true and update the token
     * @return boolean
     */
    public function checkToken() {
        $info = explode('-', $_COOKIE['autologin']);
        $user_identifier = $info[0];
        $token = $info[1];
        $query = "SELECT `session_remembered_id`, `session_remembered_token` FROM `session_remembered` WHERE `session_remembered_user_id` = '$user_identifier';";
        if (!$this->performQuery($query)) {
            die($this->getMysqliError());
        }
        $result = $this->fetchAll();
        if (!empty($result)) {
            $check = 0;
            $session_id = 0;
            foreach ($result as $row) {
                if (password_verify($token, $row['session_remembered_token'])) {
                    $check = 1;
                    $session_id = $row['session_remembered_id'];
                    break;
                }
            }
            if ($check) {
                $user_token = uniqid(rand(), TRUE);
                $hash_token = password_hash($user_token, PASSWORD_DEFAULT);
                $query = "UPDATE `session_remembered` SET `session_remembered_token`=  '' WHERE `session_remembered_id` = '$session_id';";
                if ($this->performQuery($query)) {
                    setcookie("autologin", "$user_identifier-$user_token", time() + 60 * 60 * 24 * 365, COOKIE_DIR, DOMAIN);
                }
                return $user_identifier;
            } else {
                $query = "DELETE FROM `session_remembered` WHERE `session_remembered_user_id` = '$user_identifier';";
                $this->performQuery($query);
                $query = "UPDATE `user` SET `hijacked_session_flag` = '1' WHERE `user_id` = '$user_identifier';";
                $this->performQuery($query);
                setcookie("autologin", "", time() - 60 * 60 * 24 * 365, COOKIE_DIR, DOMAIN);
                return FALSE;
            }
        } else {
            setcookie("autologin", "", time() - 60 * 60 * 24 * 365, COOKIE_DIR, DOMAIN);
        }
        return FALSE;
    }
    /**
     * It deletes the current token of user (for logout)
     */
    public function deleteToken() {
        $info = explode('-', $_COOKIE['autologin']);
        $id = $info[0];
        $token = $info[1];
        $database = new Database();
        $query = "SELECT `id`,`token` FROM `remebered_session` WHERE `user_id` = '$id';";
        $database->performQuery($query);
        $result = $database->fetchAll();
        $session_id = 0;
        $check = 0;
        foreach ($result as $row) {
            if (password_verify($token, $row['token'])) {
                $check = 1;
                $session_id = $row['id'];
                break;
            }
        }
        if (check) {
            $id = $result[0]['id'];
            $query = "DELETE FROM `remebered_session` WHERE `id` = '$session_id'";
            $database->performQuery($query);
        }
        setcookie("autologin", "", time() - 60 * 60 * 24 * 365, COOKIE_DIR, DOMAIN);
    }

}

?>
