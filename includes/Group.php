<?php


/**
 * @ClassName : Group Class
 * @Description : This Class is Used to handle the Groups
 * @Version : 1.0v
 * @LastEdit : 05/Dec/2017
 * @Author : Omar Tuffaha <omar_tuffaha@hotmail.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 */

require_once __DIR__.'/Database.php';

class Group extends Database {

    /**
     * gets all groups data owned by a user 
     * @param id $ownerId
     * @return array query result
     */
    public function fetchAllGroupsByOwner($ownerId) {
        //escape $ownerId string of illigal charecters
        $ownerId = $this->run_mysql_real_escape_string($ownerId);
        //prepare the query
        $query = "SELECT `group_info_id`, `group_info_name`, `group_info_created_date` "
                . "FROM `group_info` WHERE `group_info_owner_id` = '$ownerId';";
        if ($this->performQuery($query)) {
            return parent::fetchAll();
        } else {
            return $this->getMysqliError();
        }
    }
    /**
     * gets all groups data that a user is in
     * @param id $userId
     * @return array query result
     */
    public function fetchAllGroupsByUserIn($userId) {
        //escape $userId string of illigal charecters
        $userId = $this->run_mysql_real_escape_string($userId);
        //prepare the query
        $query = "SELECT `user_group_fk_id`, `group_info_name`, `group_info_owner_id`, `group_info_created_date` "
                . "FROM `user_group` JOIN group_info ON `user_group_fk_id` = `group_info_id` "
                . "WHERE `user_group_user_id` = '$userId';";
        if ($this->performQuery($query)) {
            return parent::fetchAll();
        } else {
            return $this->getMysqliError();
        }
    }
    
    /**
     * gets all groups data that a screen is in
     * @param id $userScreenId
     * @return array query result
     */
    public function fetchAllGroupsByScreenIn($userScreenId) {
        //escape $userScreenId string of illigal charecters
        $userId = $this->run_mysql_real_escape_string($userId);
        //prepare the query
        $query = "SELECT `screen_group_group_id`, `screen_group_user_screen_id`, `group_info_name`, `group_info_owner_id`, `group_info_created_date` "
                . "FROM `screen_group` JOIN `group_info` ON `screen_group_group_id` = `group_info_id` "
                . "WHERE `screen_group_user_screen_id` = '$userScreenId';";
        if ($this->performQuery($query)) {
            return parent::fetchAll();
        } else {
            return $this->getMysqliError();
        }
    }
    /**
     * create a group that is owned by a given user
     * @param id $ownerId
     * @param String $groupName
     * @return boolean True or False
     */
    public function createGroup($ownerId, $groupName) {
        //escape $ownerId string of illigal charecters
        $ownerId = $this->run_mysql_real_escape_string($ownerId);
        //escape $groupName string of illigal charecters
        $groupName = $this->run_mysql_real_escape_string($groupName);
        //prepare the query
        $query = "INSERT INTO `group_info`(`group_info_name`, `group_info_owner_id`) VALUES ('$groupName','$ownerId');";
        if ($this->performQuery($query)) {
            $lastId = $this->lastInsertedId();
            $query = "INSERT INTO `user_group`(`user_group_fk_id`, `user_group_user_id`) VALUES ('$lastId','$ownerId');";
            if ($this->performQuery($query)) {
                return TRUE;
            } else {
                return $this->getMysqliError();
            }
        } else {
            return $this->getMysqliError();
        }
    }
    /**
     * deletes a group from database
     * @param id $groupId
     * @return boolean true or false
     */
    public function deleteGroup($groupId) {
        //escape $groupId string of illigal charecters
        $groupId = $this->run_mysql_real_escape_string($groupId);
        //prepare the query
        $query = "DELETE FROM `group_info` WHERE `group_info_id` = '$groupId';";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

    /**
     * add a user to a group in the database
     * @param id $groupId
     * @param id $userId
     * @return boolean true or false
     */
    public function addUserToGroup($groupId, $userId) {
        //escape $ownerId string of illigal charecters
        $groupId = $this->run_mysql_real_escape_string($groupId);
        //escape $userId string of illigal charecters
        $userId = $this->run_mysql_real_escape_string($userId);
        //prepare the query
        $query = "INSERT INTO `user_group`(`user_group_fk_id`, `user_group_user_id`) VALUES ('$groupId','$userId');";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }
    
    /**
     * removes a user from a group in database
     * @param id $groupId
     * @param id $userId
     * @return boolean true or false
     */
    public function removeUserFromGroup($groupId, $userId) {
        //escape $ownerId string of illigal charecters
        $groupId = $this->run_mysql_real_escape_string($groupId);
        //escape $userId string of illigal charecters
        $userId = $this->run_mysql_real_escape_string($userId);
        //prepare the query
        $query = "DELETE FROM `user_group` WHERE `user_group_fk_id` = '$groupId' AND `user_group_user_id` = '$userId';";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

    /**
     * add screen to a group
     * @param id $groupId
     * @param id $screenId
     * @return boolean true or false
     */
    public function addScreenToGroup($groupId, $screenId) {
        //escape $groupId string of illigal charecters
        $groupId = $this->run_mysql_real_escape_string($groupId);
        //escape $screenId string of illigal charecters
        $screenId = $this->run_mysql_real_escape_string($screenId);
        //prepare the query
        $query = "INSERT INTO `screen_group`(`screen_group_group_id`, `screen_group_user_screen_id`) VALUES ('$groupId','$screenId');";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

    /**
     * remove a screen from a group
     * @param id $groupId
     * @param id $screenId
     * @return boolean true or false
     */
    public function removeScreenFromGroup($groupId, $screenId) {
        //escape $groupId string of illigal charecters
        $groupId = $this->run_mysql_real_escape_string($groupId);
        //escape $screenId string of illigal charecters
        $screenId = $this->run_mysql_real_escape_string($screenId);
        //prepare the query
        $query = "DELETE FROM `screen_group` WHERE `screen_group_group_id` = '$groupId' AND `screen_group_user_screen_id` = '$screenId';";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

}//end of class