<?php

/**
 * @ClassName : Screen Class
 * @Description : This Class is Used to handle the Screens
 * @Version : 1.0v
 * @LastEdit : 05/Dec/2017
 * @Author : Omar Tuffaha <omar_tuffaha@hotmail.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 */
require_once __DIR__ . '/Database.php';

class Screen extends Database {

    /**
     * get all screen types
     * @return array screen types
     */
    public function getAllScreenTypes() {
        $query = "SELECT `screen_type_id`, `screen_type_name`, `width`, `height` FROM `screen_type`;";
        if ($this->performQuery($query)) {
            return parent::fetchAll();
        } else {
            return $this->getMysqliError();
        }
    }

    /**
     * insert new screen type to database
     * @param String $screenType
     * @param int $width
     * @param int $height
     * @return boolean true or false
     */
    public function insertNewScreenType($screenType, $width, $height) {
        $screenType = $this->run_mysql_real_escape_string($screenType);
        $width = $this->run_mysql_real_escape_string($width);
        $height = $this->run_mysql_real_escape_string($height);
        $query = "INSERT INTO `screen_type`(`screen_type_name`, `width`, `height`) VALUES ('$screenType',$screenWidth,$screenHeight);";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

    /**
     * insert  screen to the temp table and give it a token for it to wait so a user can claim it
     * @param id $screenTypeId
     * @param String $deviceId
     * @return String token
     */
    public function insertScreenToTemp($screenTypeId, $deviceId) {
        $screenTypeId = $this->run_mysql_real_escape_string($screenTypeId);
        $deviceId = $this->run_mysql_real_escape_string($deviceId);
        $token = strtoupper(bin2hex(random_bytes(4)));
        $this->deleteOldTokens();
        $this->deleteDeviceFromTemp($deviceId);
        $query = "INSERT INTO `temp_screen`(`temp_screen_device_id`, `temp_screen_token`, `temp_screen_type_id`) VALUES ('$deviceId','$token','$screenTypeId');";
        if ($this->performQuery($query)) {
            return $token;
        } else {
            return $this->getMysqliError();
        }
    }

    public function getImageData($deviceId) {
        $deviceId = $this->run_mysql_real_escape_string($deviceId);
        $query = "SELECT `user_screen_image`,`user_screen_last_update` FROM `user_screen` WHERE `user_screen_device_id` = '$deviceId';";
        if ($this->performQuery($query)) {
            return parent::fetchAll()[0];
        } else {
            return NULL;
        }
    }
    public function getImage($deviceId) {
        $deviceId = $this->run_mysql_real_escape_string($deviceId);
        $query = "SELECT `user_screen_image` FROM `user_screen` WHERE `user_screen_device_id` = '$deviceId';";
        if ($this->performQuery($query)) {
            return parent::fetchAll()[0]['user_screen_image'];
        } else {
            return NULL;
        }
    }

    /**
     * delete a screen from temp table
     * @param id $deviceId
     * @return boolean true or false
     */
    private function deleteDeviceFromTemp($deviceId) {
        $deviceId = $this->run_mysql_real_escape_string($deviceId);
        $query = "DELETE FROM `temp_screen` WHERE `temp_screen_device_id` = '$deviceId';";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

    /**
     * delete old tokens that are older than 5 min
     * @return boolean true or false
     */
    private function deleteOldTokens() {
        $query = "DELETE FROM `temp_screen` WHERE (`temp_screen_add_timestamp`+60*5) < CURRENT_TIMESTAMP;";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

    /**
     * delete a screen type
     * @param id $screenId
     * @return boolean true or false
     */
    public function deleteScreen($screenId) {
        $screenId = $this->run_mysql_real_escape_string($screenId);
        $query = "DELETE FROM `screen_type` WHERE `screen_type_id` = '$screenId';";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

    public function deleteScreenFromUser($screenId) {
        $screenId = $this->run_mysql_real_escape_string($screenId);
        $query = "DELETE FROM `user_screen` WHERE `user_screen_device_id` = '$screenId';";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

}

//end of Screen class
