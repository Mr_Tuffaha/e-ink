<?php

/**
 * @ClassName : User Class
 * @Description : This Class is Used to handle the Users
 * @Version : 1.0v
 * @LastEdit : 05/Dec/2017
 * @Author : Omar Tuffaha <omar_tuffaha@hotmail.com>
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 */
require_once __DIR__ . '/Database.php';

class User extends Database {

    private $email;
    private $username;
    private $password;

    /**
     * username setter method
     * @param String $username
     */
    public function setUsername($username) {
        $this->username = $this->run_mysql_real_escape_string($username);
    }

    /**
     * password setter method and then hash it
     * @param String $password
     */
    public function setPassword($password) {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * email setter method
     * @param String $email
     */
    public function setEmail($email) {
        $this->email = $this->run_mysql_real_escape_string($email);
    }

    /**
     * get all users data
     * @return array array of all users
     */
    public function fetchAll() {
        $query = "SELECT `user_id`, `user_email`, `user_name`, `user_password`, `hijacked_session_flag` FROM `user`;";
        if ($this->performQuery($query)) {
            return parent::fetchAll();
        } else {
            return NULL;
        }
    }

    /**
     * get specific user data by id
     * @param id $id
     * @return array user data
     */
    public function fetchById($id) {
        $id = $this->run_mysql_real_escape_string($id);
        $query = "SELECT `user_id`, `user_email`, `user_name`, `user_password`, `hijacked_session_flag` FROM `user` WHERE `user_id = '$id';";
        if ($this->performQuery($query)) {
            return parent::fetchAll()[0];
        } else {
            return NULL;
        }
    }

    public function screenChanged($name,$id) {
        $id = $this->run_mysql_real_escape_string($id);
        $name = $this->run_mysql_real_escape_string($name);
        if($this->checkScreenNameAndUserId($name,$id)){
            $time = date("Y-m-d H:i:s");
            $query = "UPDATE `user_screen` SET `user_screen_last_update`='$time' WHERE `user_screen_user_id`='$id' AND `user_screen_image` = '$name';";
            if ($this->performQuery($query)) {
                return array("status" => 200, "info" => 'done');
            } else {
                return array("status" => 400, "info" => $this->getMysqliError());
            }
        }else{
            return array("status" => 400, "info" => 'screen not yours');
        }
    }

    /**
     * get all screens owned by a user
     * @param id $id
     * @return array of screen devices
     */
    public function fetchAllUserScreensById($id) {
        $id = $this->run_mysql_real_escape_string($id);
        $query = "SELECT `user_screen_id`, `user_screen_name`, `user_screen_image`, `user_screen_type_id`, `user_screen_device_id`, `screen_type_name`, `width`, `height` "
                . "FROM `user_screen` JOIN `screen_type` ON `user_screen_type_id` = `screen_type_id` "
                . "WHERE `user_screen_user_id` = '$id';";
        if ($this->performQuery($query)) {
            return parent::fetchAll();
        } else {
            return $this->getMysqliError();
        }
    }

    /**
     * add screen to user
     * search temp table for token if found move the screen data from temp to user
     * @param id $userId
     * @param String $token
     * @return boolean true or false
     */
    public function addScreenToUser($userId, $token, $screen_name) {
        $userId = $this->run_mysql_real_escape_string($userId);
        $token = $this->run_mysql_real_escape_string($token);
        $data = $this->getScreenFromTemp($token);
        if ($data) {
            $this->deleteTempScreen($data['temp_screen_id']);
            $deviceId = $data['temp_screen_device_id'];
            $typeId = $data['temp_screen_type_id'];
            if ($this->checkDeviceUsed($deviceId)) {
                $this->removeScreenFromUser($deviceId);
            }
            $imageName = uniqid('img_');
            while (!$this->checkScreenName($imageName)) {
                $imageName = uniqid('img_');
            }
            $query = "INSERT INTO `user_screen`(`user_screen_user_id`, `user_screen_type_id`, `user_screen_device_id`, `user_screen_name`, `user_screen_image`) VALUES ('$userId','$typeId','$deviceId','$screen_name','$imageName');";
            if ($this->performQuery($query)) {
                include_once __DIR__ . '/TextToImage.php';
                $textCon = new TextToImage();
                $temp = $textCon->createImage("Connected and ready to use", 40, 800, 480);
                $temp = imagerotate($temp, 90, 0);
                imagepng($temp,__DIR__."/../img/".$imageName.".png");
                return array('status'=>'200','info'=>'');
            } else {
                return array('status'=>'400','info'=>$this->getMysqliError());
            }
        } else {
            return array('status'=>'400','info'=>'token not found');
        }
    }

    /**
     * search temp table by token to find screen
     * @param String $token
     * @return arra screen data
     */
    private function getScreenFromTemp($token) {
        $token = $this->run_mysql_real_escape_string($token);
//        $query = "SELECT `temp_screen_id`, `temp_screen_device_id`, `temp_screen_type_id` FROM `temp_screen` WHERE `temp_screen_token` = '$token' AND (`temp_screen_add_timestamp`+60*6) < CURRENT_TIMESTAMP;";
        $query = "SELECT `temp_screen_id`, `temp_screen_device_id`, `temp_screen_type_id` FROM `temp_screen` WHERE `temp_screen_token` = '$token' AND (UNIX_TIMESTAMP(`temp_screen_add_timestamp`)+600) > UNIX_TIMESTAMP(CURRENT_TIME);";
        if ($this->performQuery($query)) {
            return parent::fetchAll()[0];
        } else {
            return NULL;
        }
    }

    /**
     * delete specific temp screen from temp table
     * @param id $tempId
     * @return boolean
     */
    private function deleteTempScreen($tempId) {
        $tempId = $this->run_mysql_real_escape_string($tempId);
        $query = "DELETE FROM `temp_screen` WHERE `temp_screen_id` = '$tempId';";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * check if any user has this device by device id
     * @param id $screenId
     * @return boolean
     */
    private function checkDeviceUsed($screenId) {
        $screenId = $this->run_mysql_real_escape_string($screenId);
        $query = "SELECT `user_screen_id` FROM `user_screen` WHERE `user_screen_device_id` = '$screenId';";
        if ($this->performQuery($query)) {
            if (parent::fetchAll()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * delete screen from user
     * @param id $screenId
     * @return boolean
     */
    public function removeScreenFromUser($screenId) {
        $screenId = $this->run_mysql_real_escape_string($screenId);
        $query = "DELETE FROM `user_screen` WHERE `user_screen_device_id` = '$screenId';";
        if ($this->performQuery($query)) {
            return TRUE;
        } else {
            return $this->getMysqliError();
        }
    }

    /**
     * check if an id is used by another user
     * @param id $id
     * @return bool
     */
    private function checkId($id) {
        $id = $this->run_mysql_real_escape_string($id);
        $query = "SELECT `user_id` FROM `user` WHERE `user_id` = '$id';";
//        var_dump($query);
        if (!$this->performQuery($query)) {
            die($this->getMysqliError());
        }
        $result = parent::fetchAll()[0];
        return $result ? FALSE : TRUE;
    }

    /**
     * check if an id is used by another user
     * @param id $id
     * @return bool
     */
    private function checkUsername($username) {
        $username = $this->run_mysql_real_escape_string($username);
        $query = "SELECT `user_name` FROM `user` WHERE `user_name` = '$username';";
//        var_dump($query);
        if (!$this->performQuery($query)) {
            die($this->getMysqliError());
        }
        $result = parent::fetchAll()[0];
        return $result ? FALSE : TRUE;
    }

    /**
     * check if an id is used by another user
     * @param id $id
     * @return bool
     */
    private function checkEmail($email) {
        $email = $this->run_mysql_real_escape_string($email);
        $query = "SELECT `user_email` FROM `user` WHERE `user_email` = '$email';";
//        var_dump($query);
        if (!$this->performQuery($query)) {
            die($this->getMysqliError());
        }
        $result = parent::fetchAll()[0];
        return $result ? FALSE : TRUE;
    }

    private function checkScreenName($screenName) {
        $screenName = $this->run_mysql_real_escape_string($screenName);
        $query = "SELECT `user_screen_image` FROM `user_screen` WHERE `user_screen_image` = '$screenName" . '.png' . "';";
//        var_dump($query);
        if (!$this->performQuery($query)) {
            die($this->getMysqliError());
        }
        $result = parent::fetchAll()[0];
        return $result ? FALSE : TRUE;
    }
    public function checkScreenNameAndUserId($screenName,$userId) {
        $screenName = $this->run_mysql_real_escape_string($screenName);
        $userId = $this->run_mysql_real_escape_string($userId);
        $query = "SELECT `user_screen_image` FROM `user_screen` WHERE `user_screen_image` = '$screenName' AND `user_screen_user_id` = '$userId';";
//        var_dump($query);
        if (!$this->performQuery($query)) {
            die($this->getMysqliError());
        }
        $result = parent::fetchAll()[0];
        return $result ? TRUE : FALSE;
    }

    /**
     * create a new user
     * @return boolean
     */
    public function createUser() {
        $user_id = uniqid(rand(), TRUE);
        while (!$this->checkId($user_id)) {
            $user_id = uniqid(rand(), TRUE);
        }
        if ($this->checkUsername($this->username)) {
            if ($this->checkEmail($this->email)) {
                $query = "INSERT INTO `user`(`user_id`, `user_email`, `user_name`, `user_password`) VALUES ('$user_id','$this->email','$this->username','$this->password');";
                if (!$this->performQuery($query)) {
                    return array("status" => 0, "data" => $this->getMysqliError());
                } else {
                    return array("status" => 1, "data" => "");
                }
            } else {
                return array("status" => 0, "data" => "email used");
            }
        } else {
            return array("status" => 0, "data" => "username used");
        }
    }

    /**
     * delete user
     * @param id $id
     */
    public function deleteUser($id) {
        $id = $this->run_mysql_real_escape_string($id);
        $query = "DELETE FROM `user` WHERE `user_id` = '$id';";
        if (!$this->performQuery($query)) {
            die($this->getMysqliError());
        }
    }

    /**
     * update user data
     * @param id $id
     * @return boolean
     */
    public function updateUser($id) {
        $id = $this->run_mysql_real_escape_string($id);
        $updateCounter = 0;
        $query = "UPDATE `user` SET";
        if (!empty($this->email)) {
            $query .= " `user_email`='$this->email'";
            $updateCounter++;
        }
        if (!empty($this->username)) {
            $query .= $updateCounter > 0 ? ", `user_name`='$this->username'" : " `user_name`='$this->username'";
            $updateCounter++;
        }
        if (!empty($this->password)) {
            $query .= $updateCounter > 0 ? ", `user_password`='$this->password'" : " `user_password`='$this->password'";
            $updateCounter++;
        }
        if ($updateCounter != 0) {
            $query .= " WHERE `user_id` = '$id';";
            if (!$this->performQuery($query)) {
                die($this->getMysqliError());
            }
        } else {
            return FALSE;
        }
    }

    /**
     * check if login params are valid
     * @param String $username
     * @param String $password
     * @param boolean $remeberMe
     * @return boolean
     */
    public function auth($username, $password, $remeberMe) {
        $username = $this->run_mysql_real_escape_string($username);
        $password = $this->run_mysql_real_escape_string($password);
        $query = "SELECT `user_id`, `user_email`, `user_name`, `user_password`, `hijacked_session_flag` FROM `user` WHERE `user_name` = '$username';";
        if ($this->performQuery($query)) {
            $thisUser = parent::fetchAll()[0];
            if (empty($thisUser)) {
                return FALSE;
            } else {
                if (password_verify($password, $thisUser['user_password'])) {
                    if ($remeberMe) {
                        include_once "Autologin.php";
                        $autologin = new Autologin();
                        $autologin->saveToken($thisUser['user_id']);
                    }
                    return $thisUser;
                } else {
                    return FALSE;
                }
            }
        }
    }

}
