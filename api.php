<?php

include_once __DIR__ . '/includes/Screen.php';
if (isset($_GET['reset']) && isset($_GET['device'])) {
    $screen = new Screen();
    $screen->deleteScreenFromUser($_GET['device']);
}
if (isset($_GET['device'])) {
    $screen = new Screen();
    $imgData = $screen->getImageData($_GET['device']);
    if ($imgData['user_screen_image']) {
        if(isset($_GET['image'])){
            $imageFile = imagecreatefrompng('img/' . $imgData['user_screen_image'] . '.png');
            header('Content-type: image/png');
            imagepng($imageFile);
        }else{
            $md5sum = md5_file('img/' . $imgData['user_screen_image'] . '.epd');
            $filesend = base64_encode(file_get_contents('img/' . $imgData['user_screen_image'] . '.epd'));
            header("Content-length: ".strlen($filesend));
            echo json_encode(array('status'=>'exist','lastUpdate'=>$imgData['user_screen_last_update'],'md5sum' => $md5sum,'file'=> $filesend));
        }
    } else {
        $token = $screen->insertScreenToTemp(1, $_GET['device']);
        include_once __DIR__ . '/includes/TextToImage.php';
        $textCon = new TextToImage();
        $textImg = $textCon->createImage("$token", 70, 800, 480);
        // $textCon->saveAsPng("img/temp");
        include_once __DIR__ . '/includes/image_functions.php';
        $eink = new E_Ink_Screen();
        $imageFile = $textImg;
        $imageFile = imagerotate($imageFile, 90, 0);
        $eink->convertImageToBlackAndWhite($imageFile);
        $array = $eink->getEPDimageDataArray($imageFile);
        $tempByteArray = "";
        foreach ($array as $byte) {
            $tempByteArray .= chr($byte);
        }
        if(isset($_GET['image'])){
            header('Content-type:image/png');
            imagepng($imageFile);
        }else{
            $md5sum = md5($tempByteArray);
            $filesend = base64_encode($tempByteArray);
            header("Content-length: ".strlen($filesend));
            echo json_encode(array('status'=>'new','md5sum' => $md5sum,'file'=> $filesend));
            // echo $filesend;
        }
    }
}
?>
