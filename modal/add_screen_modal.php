
<!-- The Modal -->
<div id="add-screen-modal" class="add-modal">

    <!-- Modal content -->
    <div id="add-screen-modal-content" class="modal-content">
        <div class="modal-header">
            <h2>Add new screen</h2>
        </div>
        <div class="modal-body">
            <div class="add">
                <form class="add-screen-form" id="add-screen-form" action="" method="post">
                    <div class="form-element">
                        <label for="">screen Token:</label>
                        <input type="text" name="screen-token" value="" placeholder="screen Name">
                    </div>
                    <div class="form-element">
                        <label for="">screen Name:</label>
                        <input type="text" name="screen-name" value="" placeholder="Number of pills in sachet">
                    </div>
                </form>
            </div>
            <div class="loading">
                <i class="fas fa-circle-notch"></i>
                <p>loading ...</p>
            </div>
            <div class="done">
                <div class="good">
                    <i class="far fa-check-circle"></i>
                    <p>Done</p>
                </div>
                <div class="error">
                    <i class="fas fa-exclamation-triangle"></i>
                    <p></p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="add">
                <button type="button" class="submit-btn btn" id="add-screen-submit-btn" name="screen-submit">Add screen</button>
                <button type="button" class="del-btn btn" id="add-screen-cancle-btn" name="add-screen-cancle-btn">Cancle</button>
            </div>
            <div class="done">
                <button type="button" class="submit-btn btn" id="add-screen-close-btn" name="">Close</button>
            </div>
        </div>
    </div>

</div>
