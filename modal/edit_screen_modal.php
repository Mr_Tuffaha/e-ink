<!-- The Modal -->
<div id="edit-screen-modal" class="edit-modal">

    <!-- Modal content -->
    <div id="edit-screen-modal-content" class="modal-content">
        <div class="modal-header">
            <h2>edit new screen</h2>
        </div>
        <div class="modal-body">
            <div class="edit">
                <form class="edit-form" method="post" enctype="multipart/form-data">
                    <input type="file" id="edit-image-modul-file" name="" value="" onchange="viewImage(this)">
                    <button type="button" class="submit-btn btn" id="edit-screen-submit-btn" name="screen-submit">Update screen</button>
                    <button type="button" class="del-btn btn" id="edit-screen-cancle-btn" name="edit-screen-cancle-btn">Cancle</button>
                </form>
                <img id="edit-image-modul-display" src="" alt="image" alt="">
            </div>
            <div class="loading">
                <i class="fas fa-circle-notch"></i>
                <p>loading ...</p>
            </div>
            <div class="done">
                <div class="good">
                    <i class="far fa-check-circle"></i>
                    <p>Done</p>
                </div>
                <div class="error">
                    <i class="fas fa-exclamation-triangle"></i>
                    <p></p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="done">
                <button type="button" class="submit-btn btn" id="edit-screen-close-btn" name="">Close</button>
            </div>
        </div>
    </div>

</div>
