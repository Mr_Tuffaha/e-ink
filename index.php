<?php
include_once __DIR__.'/php_head.php';
$pageName = "inventory";
$title = "";
$dicription = "";

include_once __DIR__.'/header.php';
include_once __DIR__.'/aside.php';
include_once __DIR__.'/modal/add_screen_modal.php';
include_once __DIR__.'/modal/edit_screen_modal.php';
?>


<main class="body-main">
    <h1>Inventory</h1>
    <div class="divider"></div>
    <table id="myTable" class="display .compact .nowrap">

    </table>
    <button id="add-screen-btn" class="add-screen-btn btn">Add New Screen</button>

</main>

<?php
include_once 'footer.php';
include_once 'scripts_and_end_page.php';
?>
