function viewImage(input) {
    // console.log(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#edit-image-modul-display').attr("src",e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readFile(file, onLoadCallback){
    var reader = new FileReader();
    reader.onload = onLoadCallback;
    reader.readAsDataURL(file);
}

function editScreenModal(table,data){
    $('#edit-image-modul-display').attr("src","img/"+data[1]+".png");
    $('#edit-screen-cancle-btn, #edit-screen-close-btn').on('click',this,function() {
        $('#edit-screen-submit-btn').off();
        $('#edit-screen-close-btn').off();
        $('#edit-screen-cancle-btn').off();
        $('.edit-modal').css('display','none');
    });


    $('#edit-screen-submit-btn').on('click',this,function() {

        var editModal = $('#edit-screen-modal');
        var imageData = $('#edit-image-modul-file')[0];
        // console.log(imageData.value);
        if(imageData.value){
            var imageFile = imageData.files[0];
            if(imageFile.type.match('image.*')){
                    editModal.find(".edit").css('display', 'none');
                    editModal.find(".loading").css('display', 'grid');
                    editModal.find(".done .error").css('display', 'none');
                    editModal.find(".done .good").css('display', 'none');
                    editModal.find(".done ").css('display', 'none');
                readFile(imageFile, function(e) {
                    var basedata = e.target.result;
                    $.ajax({
                        url: "ajax/ajaxEditScreen.php",
                        method: "POST",
                        data: { name: data[1],data: basedata },
                        contentType: "application/x-www-form-urlencoded"
                    }).done(function(data,status) {
                        table.ajax.reload(function() {
                            editModal.find(".edit").css('display', 'none');
                            editModal.find(".loading").css('display', 'none');
                            editModal.find(".done .error").css('display', 'none');
                            editModal.find(".done .good").css('display', 'grid');
                            editModal.find(".done ").css('display', 'grid');
                            $('.edit-image').off();
                            $('.edit-image').on('click',this,function() {
                                editScreenModal(table,table.row($(this).parent().parent()).data());
                            });
                        });
                    }).fail(function(data,status){
                        console.log(data.status);
                        console.log(data.statusText);
                        editModal.find(".edit").css('display', 'none');
                        editModal.find(".loading").css('display', 'none');
                        editModal.find(".done .error p").text('Error '+data.status+': '+data.statusText);
                        editModal.find(".done .error").css('display', 'grid');
                        editModal.find(".done .good").css('display', 'none');
                        editModal.find(".done ").css('display', 'grid');
                        $('.edit-image').off();
                        $('.edit-image').on('click',this,function() {
                            editScreenModal(table,table.row($(this).parent().parent()).data());
                        });
                    });
                });
            }
        }
    });

    $('.edit-modal').find(".edit").css('display', 'grid');
    $('.edit-modal').find(".loading").css('display', 'none');
    $('.edit-modal').find(".done .error").css('display', 'none');
    $('.edit-modal').find(".done .good").css('display', 'none');
    $('.edit-modal').find(".done ").css('display', 'none');
    $('.edit-modal').css('display','grid');
}
