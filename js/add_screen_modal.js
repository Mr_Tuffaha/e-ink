function addScreenModal(table){
    $('#add-screen-cancle-btn, #add-screen-close-btn').on('click',this,function() {
        $(".msg").remove();
        $('input[type=text]').removeClass('input-error');
        $('#add-screen-cancle-btn, #add-screen-close-btn, #add-screen-submit-btn').off();
        $('#add-screen-submit-btn').off();
        $('.add-modal').css('display','none');
    });


    $('#add-screen-submit-btn').on('click',this,function() {
        $(".msg").remove();
        $('input[type=text]').removeClass('input-error');
        var emptyVar = $('#add-screen-form').find('input[type=text][class!=optional]').filter(function() { return $(this).val() == ""; });
        if(emptyVar.length){
            $('#add-screen-modal-content > .modal-header > h2').after(showMsg("Empty Fields","error"));
            emptyVar.addClass('input-error');
        }else{
            var token = $('#add-screen-form').find('input[type=text][name=screen-token]').val();
            var name = $('#add-screen-form').find('input[type=text][name=screen-name]').val();
            var addModal = $('#add-screen-modal');
            addModal.find(".add").css('display', 'none');
            addModal.find(".loading").css('display', 'grid');
            addModal.find(".done").css('display', 'none');
            $.ajax({
                url: "ajax/ajaxAddScreen.php?token="+token+"&name="+name
            }).done(function(data,status) {
                table.ajax.reload(function() {
                    addModal.find(".add").css('display', 'none');
                    addModal.find(".loading").css('display', 'none');
                    addModal.find(".done .error").css('display', 'none');
                    addModal.find(".done .good").css('display', 'grid');
                    addModal.find(".done ").css('display', 'grid');
                 });
            }).fail(function(data,status){
                console.log(data);
                console.log(status);
                addModal.find(".add").css('display', 'none');
                addModal.find(".loading").css('display', 'none');
                addModal.find(".done .error p").text('Error '+data.status+': '+data.statusText);
                addModal.find(".done .error").css('display', 'grid');
                addModal.find(".done .good").css('display', 'none');
                addModal.find(".done ").css('display', 'grid');
            });
        }
    });

    $('.add-modal').find(".add").css('display', 'grid');
    $('.add-modal').find(".loading").css('display', 'none');
    $('.add-modal').find(".done .error").css('display', 'none');
    $('.add-modal').find(".done .good").css('display', 'none');
    $('.add-modal').find(".done ").css('display', 'none');
    $('.add-modal').css('display','grid');
}
