function setOnClicks(table) {

    $('#add-screen-btn').on('click',this,function() {
        addScreenModal(table);
    });
    $('.edit-image').on('click',this,function() {
        editScreenModal(table,table.row($(this).parent().parent()).data());
    });
    $('#add-screen-btn').prop('disabled', false);
}


$(document).ready(function () {



    $('#add-screen-btn').prop('disabled', true);
    var table = $('#myTable').DataTable({
        "ajax" : "ajax/ajaxRequest.php",
        columns: [
            { title: "Screen name","data": 0  },
            { title: "Screen image","data": 1  },
            { title: "Options","data": 3}
        ],
        responsive: true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'lfrtip',
        "language": {
        "emptyTable":     "No screens that belongs to you have been found"
    },
    "fnInitComplete": function() {

        setOnClicks(table);
        $('#myTable tbody').tooltip({
        items: 'tr',
        // track:true,
        position:{
            my: "center top-19",
            at: "center top-19"
        },
        show: {
            delay: 1500,
            duration: 0
          },
        content: function(){
            var imgname = table.row(this).data()[1];
            var image ="<img class='' src='img/"+imgname+".png' width='250' alt=''>";
            return image;
        }
    }); },
    });





});
