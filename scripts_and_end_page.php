</div>
<!-- closing body-container -->
<script>
function showMsg(msg,type) {
    var error = '<div class="error-msg msg"><i class="fa fa-times-circle"></i>'+msg+'</div>';
    var warning = '<div class="warning-msg msg"><i class="fa fa-warning"></i>'+msg+'</div>';
    var success = '<div class="success-msg msg"><i class="fa fa-check"></i>'+msg+'</div>';
    var info = '<div class="info-msg msg"><i class="fa fa-info-circle"></i>'+msg+'</div>';
    switch (type) {
        case 'error':
        return error;
        break;
        case 'warning':
        return warning;
        break;
        case 'success':
        return success;
        break;
        case 'info':
        return info;
        break;
        default:
    }
}

</script>
<?php
echo "<script src='https://code.jquery.com/jquery-3.3.1.min.js' integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=' crossorigin='anonymous'></script>\n";
switch ($pageName) {
    case 'inventory':
        echo "<script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js' integrity='sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=' crossorigin='anonymous'></script>\n";
        echo "<script src='https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-html5-1.5.1/b-print-1.5.1/datatables.min.js'></script>\n";
        echo "<script src='js/add_screen_modal.js'></script>";
        echo "<script src='js/edit_screen_modal.js'></script>";
        echo "<script src='js/index.js'></script>";
        break;
    default:
        # code...
        break;
}
 ?>

</body>
</html>
