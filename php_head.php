<?php
include_once __DIR__.'/includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain ,$https);
if (!isset($_SESSION['user_id'])) {
    header("location: login.php");
}

 ?>
