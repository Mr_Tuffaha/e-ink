<?php
//var_dump($_SERVER['HTTP_REFERER']);
include_once __DIR__.'/../includes/SessionManager.php';
include_once __DIR__.'/../includes/config.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain ,$https);
if(!isset($_SESSION['user_id'])){
    header("HTTP/1.0 401");
    die();
}

if(isset($_SERVER['HTTP_REFERER'])&&parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == DOMAIN){
    include_once __DIR__.'/../includes/User.php';
    $user = new User();
    // var_dump($_SERVER['REQUEST_METHOD']);
    if (!empty($_POST['data']) && !empty($_POST['name'])) {
        if ($user->checkScreenNameAndUserId($_POST['name'], $_SESSION['user_id'])) {
            $file =  explode(",",$_POST['data']);
            $file =  base64_decode($file[1]);
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (in_array($finfo->buffer($file), array('image/png', 'image/jpg', 'image/jpeg','image/gif'))) {
                $image = imagecreatefromstring($file);
                $image = imagescale($image, 480, 800);
                include_once __DIR__.'/../includes/image_functions.php';
                $eink = new E_Ink_Screen();
                $eink->convertImageToBlackAndWhite($image, 0);
                imagepng($image,__DIR__.'/../img/'.$_POST['name'].'.png');
                $array = $eink->getEPDimageDataArray($image);
                $file = fopen(__DIR__.'/../img/' . $_POST['name'] . '.epd', "wb");
                foreach ($array as $byte) {
                    fwrite($file, chr($byte));
                }
                fclose($file);
                $result = $user->screenChanged($_POST['name'], $_SESSION['user_id']);
                header("HTTP/1.0 {$result['status']} {$result['info']}");
            }else{
                header("HTTP/1.0 400 incorrect input");
                die();
            }
        }
    }else{
        header("HTTP/1.0 400 incorrect input");
        die();
    }
}else{
   header("HTTP/1.0 403 forbiden");
    die();
}
