<?php
include_once __DIR__.'/../includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain ,$https);
if(isset($_SESSION['user_id'])){
    include_once __DIR__.'/../includes/User.php';
    $user = new User();
    $screens = $user->fetchAllUserScreensById($_SESSION['user_id']);
    if($screens){
        foreach ($screens as $row) {
            # code...
            $options = "<p class='edit-image'>edit image</p>";
            $data['data'][] = array($row['user_screen_name'],$row['user_screen_image'],$row['user_screen_id'],$options);;
        }
        // $data['data'] = $screens;

    }else{
        $data['data'] = false;
    }
    // $data['data'] = $screens;
    header('Content-Type: application/json');
    echo json_encode($data);
    // echo json_encode($data);
}else{
    header("HTTP/1.0 401");
    die();
}
?>
