<?php
include_once __DIR__.'/../includes/SessionManager.php';
include_once __DIR__.'/../includes/config.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain ,$https);
if(!isset($_SESSION['user_id'])){
    header("HTTP/1.0 401");
    die();
}

if(isset($_SERVER['HTTP_REFERER'])&&parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == DOMAIN){
    include_once __DIR__.'/../includes/User.php';
    $user = new User();
    if (!empty($_GET['token']) && !empty($_GET['name'])) {
        $result = $user->addScreenToUser($_SESSION['user_id'], $_GET['token'], $_GET['name']);
        var_dump($result);
        header("HTTP/1.0 {$result['status']} {$result['info']}");
        die();
    }else{
        header("HTTP/1.0 400 incorrect input");
        die();
    }
}
