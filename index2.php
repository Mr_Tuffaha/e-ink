<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="author" content="Omar Tuffaha">
        <meta name="description" content="">
        <meta name="keywords" content="Omar Tuffaha">
        <meta property="og:image" content="">
        <meta property="og:description" content="">
        <meta property="og:title" content="">
        <title>E-ink</title>
        <link rel="stylesheet" href="css/index2.css">
        <title></title>
         <link href="https://fonts.googleapis.com/css?family=Cousine:400,700" rel="stylesheet">
        <!-- <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700" rel="stylesheet"> -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet"> -->
        <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet">
    </head>
    <body>
        <div class="screen">
            <div id="screen_1" class="screen-containor">
                <h1>Omar Hiari, Ph.D.</h1>
                <div class="hr"></div>
                <h2>Computer Engineering Department</h2>
                <h2>School of Electrical Engineering and Information Technology</h2>
                <div class="hr"></div>
                <h3><i class="fas fa-paperclip fa-lg"></i></i>Grades</h3>
                <p class="center">Scan the appropriate QR code for your course</p>
                <div class="grades-QR">
                    <div class="course">
                        <h4>CS###</h4>
                        <img src="img/qr_code.jpeg" alt="qr-code">
                    </div>
                    <div class="course">
                        <h4>CS###</h4>
                        <img src="img/qr_code.jpeg" alt="qr-code">
                    </div>
                    <div class="course">
                        <h4>CS###</h4>
                        <img src="img/qr_code.jpeg" alt="qr-code">
                    </div>
                    <div class="course">
                        <h4>CS###</h4>
                        <img src="img/qr_code.jpeg" alt="qr-code">
                    </div>
                </div>
                <div class="hr"></div>
                <h3><i class="fas fa-rss fa-lg"></i></i>Announcements</h3>
                <p><i class="far fa-dot-circle"></i> Announcement 1</p>
                <p><i class="far fa-dot-circle"></i> Announcement 2</p>
                <p><i class="far fa-dot-circle"></i> Announcement 3</p>
                <p><i class="far fa-dot-circle"></i> Announcement 4</p>
                <p><i class="far fa-dot-circle"></i> Announcement 5</p>
                <div class="hr"></div>
                <h3><i class="far fa-question-circle fa-lg"></i>Current Status</h3>
                <p class="status center">Available</p>
            </div>
            <button type="button" id="capture_1" name="button">capture</button>
        </div>
        <div class="screen">
            <div id="screen_2" class="screen-containor">
                <h1>Omar Hiari, Ph.D.</h1>
                <div class="hr"></div>
                <h2>Computer Engineering Department</h2>
                <h2>School of Electrical Engineering and Information Technology</h2>
                <div class="hr"></div>
                <h3><i class="far fa-calendar-alt fa-lg"></i></i></i>Schedule</h3>
                <table width="100%" class="table-schedule">
                    <thead>
                        <tr>
                            <th></th>
                            <th>8:00-9:30</th>
                            <th>9:30-11:00</th>
                            <th>11:00-12:30</th>
                            <th>12:30-2:00</th>
                            <th>2:00-5:00</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Sun</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Mon</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Tue</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Wed</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Thu</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>

                <div class="hr"></div>
                <h3><i class="fas fa-rss fa-lg"></i></i>Announcements</h3>
                <p><i class="far fa-dot-circle"></i> Announcement 1</p>
                <p><i class="far fa-dot-circle"></i> Announcement 2</p>
                <p><i class="far fa-dot-circle"></i> Announcement 3</p>
                <p><i class="far fa-dot-circle"></i> Announcement 4</p>
                <p><i class="far fa-dot-circle"></i> Announcement 5</p>
                <div class="hr"></div>
                <h3><i class="far fa-question-circle fa-lg"></i>Current Status</h3>
                <p class="status center">Available</p>
            </div>
            <button type="button" id="capture_2" name="button">capture</button>
        </div>


        <script src="https://code.jquery.com/jquery-3.2.1.min.js"
                integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
        <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> -->
        <script src="js/html2canvas.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.svg.js"></script> -->
        <script>
            $(document).ready(function () {
                $('#capture_2').click(function () {
                    html2canvas(document.getElementById("screen_2"),{
                        scale:2
                    }).then(function (canvas) {
                        document.body.appendChild(canvas);
                    });
                });

                $('#capture_1').click(function () {
                    html2canvas(document.getElementById("screen_1"),{
                        scale:2
                    }).then(function (canvas) {

                        document.body.appendChild(canvas);
                        // var dataURL = canvas.toDataURL("image/png");
                        // $('#img_val').val(dataURL);
                        // console.log($('#img_val'));
                        // $('#imageToPi').submit();
                    });
                });
            });
        </script>
    </body>
</html>
