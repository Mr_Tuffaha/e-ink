<aside class="body-aside">
    <img src="img/profile.png" class="profile-image" alt="Profile Image">
    <h3 class="username"><?php echo isset($_SESSION['user_id'])?$_SESSION['user_name']:'';?></h3>
    <div class="divider"></div>
    <div class="menu-link-container">
        <a class="menu-link" href="index.php">My Screens</a>
        <a class="menu-link" href="signout.php">Sign out</a>
    </div>
</aside>
