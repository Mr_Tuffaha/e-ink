<?php
session_start();
if (!isset($_SESSION['user_id'])) {
    header("location: login.php");
}

include_once 'includes/image_functions.php';
include_once 'includes/User.php';
$user = new User();
if (isset($_POST['submitScreen'])) {
    if (!empty($_POST['screen_temp_id'] && !empty($_POST['screen_name']))) {
        $result = $user->addScreenToUser($_SESSION['user_id'], $_POST['screen_temp_id'], $_POST['screen_name']);
        if ($result === TRUE) {
            $message = "done";
        } else if ($result === FALSE) {
            $message = "id not found";
        } else {
            $message = $result;
        }
    }
} else if (isset($_POST['btnsubmit'])) {

    if (isset($_FILES)) {
        if($_FILES['image']['name']){
            $fileData['image_name'] = $_FILES['image']['name'];
            $fileData['image_ext'] = strtolower(pathinfo($_FILES['image']['name'])['extension']);
            $fileData['image_content'] = $_FILES['image']['tmp_name'];
            $fileData['image_size'] = $_FILES['image']['size'];
            if (in_array($fileData['image_ext'], array('png', 'jpg', 'jpeg'))) {
                if ($fileData['image_size'] < 5000000) {
                    move_uploaded_file($fileData['image_content'], 'img/image_to_convert.' . $fileData['image_ext']);
                    //  $file = fopen("img/image_ext.txt", "w");
                    //fwrite($file, $fileData['image_ext']);
                    //fclose($file);
                    //original image
                    $image = null;
                    switch ($fileData['image_ext']) {
                        case 'png':
                        $image = imagecreatefrompng('img/image_to_convert.png');
                        break;
                        case 'jpg':
                        $image = imagecreatefromjpeg('img/image_to_convert.jpg');
                        break;
                        case 'jpeg':
                        $image = imagecreatefromjpeg('img/image_to_convert.jpeg');
                        break;
                    }

                    if (isset($_POST['invert'])) {
                        $invert = 1;
                    } else {
                        $invert = 0;
                    }
                    $image = imagescale($image, 480, 800);
                    $eink = new E_Ink_Screen();
                    imagefilter($image, IMG_FILTER_CONTRAST, 70);

                    $eink->convertImageToBlackAndWhite($image, $invert);
                    //$user = new User();
                    $imgName = $_POST['img_name'];
                    if ($user->checkScreenNameAndUserId($imgName, $_SESSION['user_id'])) {
                        imagepng($image, 'img/' . $imgName . '.png');
                        //writing to a file
                        $file = fopen('img/' . $imgName . '.epd', "wb");
                        $array = $eink->getEPDimageDataArray($image);
                        foreach ($array as $byte) {
                            fwrite($file, chr($byte));
                        }
                        fclose($file);
                        $image = $imgName;
                    } else {
                        $message = "incorrect data";
                    }
                }
            }
        }
    }
} else if (isset($_POST['img_val'])) {
    $img = $_POST['img_val'];
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = 'img/image_to_convert.png';
    $success = file_put_contents($file, $data);
    $image = imagecreatefrompng('img/image_to_convert.png');
    $image = imagescale($image, 480, 800);
    // imagefilter($image, IMG_FILTER_CONTRAST, 90);
    $eink = new E_Ink_Screen();
    $eink->convertImageToBlackAndWhite($image);
    $imgName = $_POST['img_name'];
    if ($user->checkScreenNameAndUserId($imgName, $_SESSION['user_id'])) {
        imagepng($image, 'img/' . $imgName . '.png');
        //writing to a file
        $file = fopen('img/' . $imgName . '.epd', "wb");
        $array = $eink->getEPDimageDataArray($image);
        foreach ($array as $byte) {
            fwrite($file, chr($byte));
        }
        fclose($file);
        $image = $imgName;
    } else {
        $message = "incorrect data";
    }
}
//$user = new User();
$screens = $user->fetchAllUserScreensById($_SESSION['user_id']);
if (isset($_GET['screen'])) {
    foreach ($screens as $row) {
        if ($row['user_screen_image'] == $_GET['screen']) {
            $image = $row['user_screen_image'];
        }
    }
} else {
    $image = $screens[0]['user_screen_image'];
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Welcome to E-ink system where you can add and control your `pervasive displays` ink screens">
    <meta name="keywords" content="E-ink, inkscreen, escreen, main">
    <meta name="author" content="Omar Tuffaha">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-ink System</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body class="body-container">
    <header>
        <div class="logo">
            <h1>Logo</h1>
        </div>
        <div class="links">
            <a href="#"><?php echo $_SESSION['user_name']; ?></a>
            <i class="fa fa-home" aria-hidden="true"></i>
            <i class="fa fa-user" aria-hidden="true"></i>
            <a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
        </div>
        <div class="line">
        </div>
    </header>
    <div class="screen-change">
        <div class="msg"><h3><?php echo isset($message) ? $message : ""; ?></h3></div>
        <div class="add-new">
            <form class="" id="new-screen" action="" method="post" enctype="multipart/form-data">
                <label for="temp_id">add new screen: </label>
                <input type="text" class="" id="temp_id" name="screen_temp_id" placeholder="screen temp id" value="">
                <input type="text" class="" id="temp_id" name="screen_name" placeholder="screen new name" value="">
                <input type="submit" name="submitScreen" value="add">
            </form>
        </div>
        <div class="choose-screen">
            <?php
            //$user = new User();
            //$screens = $user->fetchAllUserScreensById($_SESSION['user_id']);
            ?>
            <label for="screenId">choose screen</label>
            <select id="screenId" name="screen">
                <?php
                if (isset($_GET['screen'])) {
                    foreach ($screens as $row) {
                        if ($_GET['screen'] == $row['user_screen_image']) {
                            echo "<option value='" . $row['user_screen_image'] . "' selected>" . $row['user_screen_name'] . "</option>";
                        } else {
                            echo "<option value='" . $row['user_screen_image'] . "'>" . $row['user_screen_name'] . "</option>";
                        }
                    }
                } else {
                    foreach ($screens as $row) {
                        echo "<option value='" . $row['user_screen_image'] . "'>" . $row['user_screen_name'] . "</option>";
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <main>
        <form class="" id="imageToPi" action="" method="post" enctype="multipart/form-data">
            <input type="file" class="file_upload" name="image" value="">
            <input type="checkbox" name="invert" value="">
            <input type="hidden" name="img_name" id="img_name" value="<?php echo $image; ?>" />
            <input type="hidden" name="img_val" id="img_val" value="" />
            <input type="submit" name="btnsubmit" value="send">
        </form>
        <input type="submit" id="capture" name="" value="capture">
    </main>

    <div class="text_to_image">

        <textarea name="content" id="editor">This is some sample content.</textarea>
        <img id="preview_image" class="preview_image" src='<?php echo isset($image) ? 'img/' . $image . '.png?' . uniqid() : ""; ?>' alt="">
    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
    <script src="js/html2canvas.js"></script>
    <!-- <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script> -->
    <script src="https://cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
    <!-- <script src="js/ckeditor.js"></script> -->
    <script>
    CKEDITOR.replace('editor', {
        width: "480px",
        height: "800px"
    });
    </script>
    <script>
    $(document).ready(function () {
        $("#screenId").change(function () {
            var id = $("#screenId").val();
            window.location.replace("index.php?screen=" + id);
        });
        $('#capture').click(function () {
            html2canvas(document.getElementById("cke_1_contents")).then(function (canvas) {
                //document.body.appendChild(canvas);
                var dataURL = canvas.toDataURL("image/png");
                $('#img_val').val(dataURL);
                // console.log($('#img_val'));
                $('#imageToPi').submit();
            });
        });
    });
    </script>
</body>
</html>
