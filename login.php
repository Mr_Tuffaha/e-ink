<?php
include_once __DIR__.'/includes/SessionManager.php';
SessionManager::sessionStart($sessionName, $lifetime, $cookiePath, $currentDomain ,$https);

include_once __DIR__ . '/includes/Autologin.php';
include_once __DIR__ . '/includes/User.php';
if (isset($_SESSION['user_id'])) {
    header("location: index.php");
}

// var_dump($_POST);
if (isset($_POST['type'])&&$_POST['type']=="signup") {
    if (!empty($_POST['email']) && !empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['repassword'])) {
        if ($_POST['password'] == $_POST['repassword']) {
            $user = new User();
            $user->setEmail($_POST['email']);
            $user->setUsername($_POST['username']);
            $user->setPassword($_POST['password']);
            $result = $user->createUser();
            if ($result['status']) {
                $sign_msg = "signed up!";
            } else {
                $sign_msg = $result['data'];
            }
        } else {
            $sign_msg = "not matching passwords";
        }
    } else {
        $sign_msg = "empty fields";
    }
} else if (isset($_POST['type'])&&$_POST['type']=="login") {
    var_dump($_POST);
    if (!empty($_POST['username']) && !empty($_POST['password'])) {
        $user = new User();
        $userInfo = $user->auth($_POST['username'], $_POST['password'], 0);
        if ($userInfo) {
            $_SESSION['user_id'] = $userInfo['user_id'];
            $_SESSION['user_name'] = $userInfo['user_name'];
            header("location: index.php");
        } else {
            $login_msg = "incorrect username and password";
        }
    } else {
        $login_msg = "empty fields";
    }
}
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Omar Tuffaha">
    <meta name="description" content="">
    <meta property="og:image" content="">
    <meta property="og:description" content="">
    <meta property="og:title" content="">
    <title>E-ink Login</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">

</head>
<body>
    <div class="body-container">
        <header class="body-header">
            <div class="header-logo">
                <h1>E-ink</h1>
                <h3></h3>
            </div>
            <div class="header-other">

            </div>
        </header>
        <main class="body-main" id="body-main">

            <div class="form-container">
                <div class="form-nav-container">
                    <button type="button" class="form-nav btn" id="login-btn" name="add">Login</button>
                    <button type="button" class="form-nav btn" id="signup-btn" name="submitbtn">Signup</button>
                </div>
                <?php echo isset($login_msg) ? "<h3 class='alert'>$login_msg</h3>" : ""; ?>
                <form class="form login-form" action="" method="post" autocomplete="off">
                    <div class="form-title">
                        <h1>Login</h1>
                    </div>
                    <input type="hidden" name="type" value="login">
                    <div class="form-fieldset">
                        <label class="form-label" for="">Username:</label>
                        <input class="form-input" type="text" name="username" value=""  placeholder="username">
                    </div>
                    <div class="form-fieldset">
                        <label class="form-label" for="">Password:</label>
                        <input class="form-input" type="password" name="password" value=""  placeholder="password">
                    </div>
                    <div class="form-submit">
                        <input class="form-input-submit btn" type="button" name="login" value="Submit">
                    </div>
                </form>


                <form class="form signup-form hide"  action="" method="post" autocomplete="off">
                    <div class="form-title">
                        <h1>Sign Up</h1>
                    </div>
                    <input type="hidden" name="type" value="signup">
                    <div class="form-fieldset">
                        <label class="form-label" for="">Email:</label>
                        <input class="form-input" placeholder="Email" type="text" name="email" value="">
                    </div>
                    <div class="form-fieldset">
                        <label class="form-label" for="">Username:</label>
                        <input class="form-input" placeholder="username" type="text" name="username" value="">
                    </div>
                    <div class="form-fieldset">
                        <label class="form-label" for="">Password:</label>
                        <input class="form-input" placeholder="password" type="password" name="password" value="">
                    </div>
                    <div class="form-fieldset">
                        <label class="form-label" for="">RePassword:</label>
                        <input class="form-input" placeholder="password" type="password" name="repassword" value="">
                    </div>
                    <div class="form-submit">
                        <input class="form-input-submit btn" type="button" name="signup" value="Submit">
                    </div>
                </form>

            </div>


        </main>
        <footer class="body-footer">
            <p id="copyright" property="dc:rights">&copy;
              <span property="dc:dateCopyrighted"><?php echo date("Y");?></span>
              <span property="dc:publisher">Omar Tuffaha</span>
            </p>
        </footer>


    </div>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/login.js"></script>
</body>
</html>
